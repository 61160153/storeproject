/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectUser;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author ADMIN
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO customer (name,tel)VALUES (?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,tel FROM customer ;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(id, name, tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id, name,tel FROM customer WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int cid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                Customer customer = new Customer(cid, name, tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
       Database db = Database.getIntstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM customer WHERE id = ? ;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

       db.close();
       return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getIntstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE customer SET name = ?,tel = ? WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setInt(3,object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectUser.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "JB", "09867854321"));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));
        Customer lastCustomer = dao.get(id);
        System.out.println("list customer: " + lastCustomer);
        lastCustomer.setTel("0922134567");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: " + updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer: " + deleteCustomer);

    }
}
